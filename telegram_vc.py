import requests
from bs4 import BeautifulSoup
import re

def get_text(url): 
#из URL вытаскиваем html
    r = requests.get(url)
    text=r.text 
    return text

def get_items(text,top_name,class_name): # функция нахождения списка последних новостей
    soup = BeautifulSoup(text, "lxml")
    news_list = soup.find('div', {'class': top_name})
    items = news_list.find_all('a', {'class': [class_name]})
    return items

def send_telegram(text: str): # функция отправки сообщения в телеграмм канал
    token = input('Введите свой токен:')
    url = "https://api.telegram.org/bot"
    channel_id = input('Введите адрес телеграм канала:')
    url += token
    method = url + "/sendMessage"

    r = requests.post(method, data={
         "chat_id": channel_id,
         "text": text
          })

    if r.status_code != 200:
        raise Exception("post_text error")
    
url ='https://vc.ru/new'

top_name = 'feed'
class_name = 'content-link'
text = get_text(url)

items = get_items(text,top_name,class_name)
result = str(items[0])
result_url = re.findall(r'https://.+[^-"></a>]', result) # вытаскиваем ссылку с помощью регулярного выражения
send_telegram(result_url)
