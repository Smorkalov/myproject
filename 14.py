import re # поиск одинаковых словв предложениях a и b и вывод самого длинного слова


a, b = input(), input()
a, b = a.lower(), b.lower()
result_a = re.findall(r'\w+-?\w+', a)
result_b = re.findall(r'\w+-?\w+', b)
st_a = set(result_a)
st_b = set(result_b)
list_c = list(st_a & st_b)
list_c = max(list_c, key=len)
print(list_c)
