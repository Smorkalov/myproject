import datetime

import sqlalchemy as sa
from sqlalchemy.orm import declarative_base, Session


Base = declarative_base()


class TimestampModelMixin:
    created_at = sa.Column(sa.DateTime, default=datetime.datetime.now)


class PrimaryKeyMixin:
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)


class Employee(TimestampModelMixin, Base):
    __tablename__ = "employees"

    position = sa.Column(sa.Unicode(64), primary_key=True)


class Salary(PrimaryKeyMixin, TimestampModelMixin, Base):
    __tablename__ = "salaries"

    employee = sa.Column(sa.ForeignKey(f"{Employee.__tablename__}.position"), nullable=False)
    salary = sa.Column(sa.Integer, sa.CheckConstraint("salary > 0"), nullable=False)


class Role:
    def __init__(self, dsn: str):
        engine = sa.create_engine(dsn)
        Base.metadata.create_all(engine)
        self.session = Session(engine)


class ManagerRole(Role):
    def get_information_employee(self, position: str) -> int:
        # информация о зарплате сотрудника
        result = self.session.query(Salary.salary).filter(Salary.employee == position)
        return result[0][0]

    def add_employee(self, position: str) -> Employee:
        # добавление должности в базу
        employee = Employee(position=position)

        with self.session:
            self.session.add(employee)
            self.session.commit()

        return employee

    def add_salary(self, position: str, salary: int) -> Salary:
        # добавление информации о зарплате в базу
        salary = Salary(employee=position, salary=salary)

        with self.session:
            self.session.add(salary)
            self.session.commit()

        return salary

    def change_salary(self, position: str, salary: int) -> int:
        # изменение зарплаты для должности. Здесь не смог найти другого способа,
        # только вытаскивал по запросу id.
        # А потом вытаскивал экземпляр и менял ему зп. Есть наверное легче пути :-)
        salary_ = self.session.query(Salary.id).filter(Salary.employee==position)
        x = self.session.query(Salary).get(salary_[0][0])
        x.salary = salary
        self.session.commit()
        return salary


Cleaner = Employee(position="Уборщица")
Cashier = Employee(position="Кассир")
Salesman = Employee(position="Продавец")

salaries = {Cleaner: 28, Cashier: 60, Salesman: 128}

manager = ManagerRole("sqlite: ///db.sqlite")
# manager = ManagerRole("sqlite:///:memory:")

for employee, salary in salaries.items():
    manager.add_employee(employee.position)
    manager.add_salary(employee.position, salary)

Animator = Employee(position="Аниматор")
manager.add_employee("Аниматор")
manager.change_salary(position="Уборщица", salary=48)
manager.get_information_employee(position="Уборщица")
